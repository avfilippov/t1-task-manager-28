package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "show task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @Nullable final Task task = getTaskService().findOneById(userId, id);
        showTask(task);
    }
}
